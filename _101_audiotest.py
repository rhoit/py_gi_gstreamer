#!/usr/bin/env python

""" 01 Audio Test
----------------------------------------------------------------------
nearest command line equivlent
$ gst-launch-0.10 audiotestsrc ! autoaudiosink
"""

from gi.repository import Gst
import time

Gst.init(None)

pipeline = Gst.Pipeline()

source = Gst.ElementFactory.make('audiotestsrc', None)
pipeline.add(source)

sink = Gst.ElementFactory.make("autoaudiosink", "sink")
pipeline.add(sink)
source.link(sink)

pipeline.set_state(Gst.State.PLAYING)
time.sleep(1)
