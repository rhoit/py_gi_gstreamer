#!/usr/bin/env python

""" 103 Tone Generator
----------------------------------------------------------------------
Read the tone from the file and play it.
"""

from gi.repository import Gst
import sys, time

def makeGstreamer():
    Gst.init(None)
    pipeline = Gst.Pipeline()

    source = Gst.ElementFactory.make('audiotestsrc', None)
    pipeline.add(source)

    sink = Gst.ElementFactory.make("autoaudiosink", "sink")
    pipeline.add(sink)
    source.link(sink)

    return pipeline, source, sink


def playFile(source, media):
    dat = open(media).read()
    for tone in dat.splitlines():
        freq, length = tone.split('\t')
        source.set_property("freq", float(freq))
        time.sleep(int(length)/1000)


def main():
    pipeline, source, sink = makeGstreamer()
    source.set_property("freq", 0)
    pipeline.set_state(Gst.State.PLAYING)
    playFile(source, sys.argv[1])


if __name__ == '__main__':
    main()
