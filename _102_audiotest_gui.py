#!/usr/bin/env python

""" 102 Audio Test GUI
----------------------------------------------------------------------
Frequency Tuner GUI
"""

from gi.repository import Gtk, Gst
import sys

class MyGUI(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Freq Tuner")
        self.connect("delete-event", Gtk.main_quit)
        self.makeGstreamer()
        self.makeWidget()
        self.show_all()


    def makeWidget(self):
        layout = Gtk.HBox(spacing=5)
        self.add(layout)

        self.icon_play = Gtk.Image.new_from_stock(Gtk.STOCK_MEDIA_PLAY, Gtk.IconSize.BUTTON)
        self.icon_pause = Gtk.Image.new_from_stock(Gtk.STOCK_MEDIA_PAUSE, Gtk.IconSize.BUTTON)

        self.b_play = Gtk.ToggleButton()
        layout.add(self.b_play)
        self.b_play.set_image(self.icon_play)
        self.b_play.connect("clicked", lambda *e: self.playPause())

        layout.add(Gtk.Label("Frequency"))

        self.frequency = Gtk.SpinButton.new_with_range(200, 1000, 50)
        layout.add(self.frequency)
        self.frequency.connect("value-changed", self.changeFreq)

        layout.add(Gtk.Label("Hz "))


    def makeGstreamer(self):
        self.pipeline = Gst.Pipeline()
        self.source = Gst.ElementFactory.make('audiotestsrc', None)
        self.pipeline.add(self.source)

        self.sink = Gst.ElementFactory.make("autoaudiosink", "sink")
        self.pipeline.add(self.sink)
        self.source.link(self.sink)


    def changeFreq(self, widget):
        self.source.set_property("freq", widget.get_value())


    def playPause(self):
        if self.pipeline.get_state(0)[1] == Gst.State.PLAYING:
            self.b_play.set_image(self.icon_play)
            self.pipeline.set_state(Gst.State.PAUSED)
            return
        self.b_play.set_image(self.icon_pause)
        self.pipeline.set_state(Gst.State.PLAYING)


def root_binds(widget, event):
    # print(event.keyval)
    if event.keyval == 65307:
        Gtk.main_quit()


def main():
    Gst.init(sys.argv)

    root = MyGUI()
    root.connect('key_release_event', root_binds)


if __name__ == '__main__':
    main()
    Gtk.main()
