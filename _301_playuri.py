#!/usr/bin/env python

""" 03 Play uri
----------------------------------------------------------------------
command line equivlent
$ gst-launch-0.10 filesrc location=song.ogg ! decodebin ! audioconvert ! alsasink
"""

from gi.repository import Gst, GObject
import os, sys

Gst.init(None)

player = Gst.ElementFactory.make('playbin', None)
player.set_property("uri", "file://" + os.path.abspath(sys.argv[1]))

player.set_state(Gst.State.PLAYING)
GObject.MainLoop().run()
