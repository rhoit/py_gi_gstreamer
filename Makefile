default: cli-tone-gen

301: test-audio-web
test-audio-web:
	./_301_playuri.py song.ogg

202: cli-test-audio-file
cli-test-audio-file:
	./_202_playfile_cli.py song.ogg

201: test-audio-file
test-audio-file:
	./_201_playfile.py song.ogg

105: gui-tone-gen
gui-tone-gen:
	./_104_tonegen_gui.py

104: cli-tone-gen
cli-tone-gen:
	./_104_tonegen_cli.py tones/mario.beep

103: tone-gen
tone-gen:
	./_103_tonegen.py tones/song.beep

phone-dialer:
	./_103_phonedialer.py

102: gui-audiotest
gui-test-audio:
	./_102_audiotest_gui.py

101: test-audio
test-audio:
	./_101_audiotest.py
