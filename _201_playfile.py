#!/usr/bin/env python

""" 02 Play file
----------------------------------------------------------------------
command line equivlent
$ gst-launch-0.10 filesrc location=song.ogg ! decodebin ! audioconvert ! alsasink
"""

from gi.repository import Gst, GObject
import sys

GObject.threads_init() # Initializing threads used by the Gst various elements

Gst.init(None)

pipeline = Gst.Pipeline()

source = Gst.ElementFactory.make('filesrc', None)
source.set_property("location", sys.argv[1])
pipeline.add(source)


def onPadAdded(element, pad):
    pad.link(sink.get_static_pad('sink'))

decoder = Gst.ElementFactory.make("decodebin", "decoder")
decoder.connect('pad-added', onPadAdded)
pipeline.add(decoder)

sink = Gst.ElementFactory.make("autoaudiosink", "sink")
pipeline.add(sink)

source.link(decoder)

pipeline.set_state(Gst.State.PLAYING)
GObject.MainLoop().run()
