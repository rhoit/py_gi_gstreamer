#!/usr/bin/env python

""" 103 Tone Generator CLI
----------------------------------------------------------------------
Play Info in Terminal
"""

import _103_tonegen as tonegen
from gi.repository import Gst
import time
import sys

def report_progress(playing, duration, width=40):
    ratio = playing/duration
    x = int(ratio * width)
    info = "%.2f/%.2f sec"%(playing, duration)
    timebar = "%s>%s"%('='*x, '-'*(width-x))
    percentage = int(ratio * 100)
    sys.stderr.write('\r ▶ %4d %% [%s] %s'%(percentage, timebar, info))
    sys.stderr.flush()


def playFile(source, media):
    dat = open(media).read()
    tones = []
    duration = 0
    for tone in dat.splitlines():
        f, l = tone.split('\t')
        length = int(l)/1000
        tones.append((float(f), length))
        duration += length

    playing = 0.0
    for i, (f, l) in enumerate(tones):
        playing += l
        report_progress(playing, duration)
        source.set_property("freq", f)
        time.sleep(l)
    report_progress(duration, duration)
    print()


def main():
    tonegen.playFile = playFile
    tonegen.main()


if __name__ == '__main__':
    main()
